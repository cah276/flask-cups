import cups
import pprint
import time

conn = cups.Connection()
printers = conn.getPrinters()

pprint.pprint(printers)

printer = conn.getDefault()
print("Default: ", printer)

if printer == None:
    printer = list(printer.keys())[0]
    print("New Default: ", printer)

myfile = "./bob.txt"

pid = conn.printFile(printer, myfile, "test", {})
while conn.getJobs().get(pid, None) is not None:
    time.sleep(1)
