from flask import Flask, render_template, flash, redirect, url_for, session, request
import os
import cups
from passlib.handlers.sha2_crypt import sha256_crypt
from wtforms import Form, StringField, PasswordField, validators
from sqlalchemy import create_engine, Column, Integer, String
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker, scoped_session
from sqlalchemy_mixins import AllFeaturesMixin
from functools import wraps
import time

Base = declarative_base(name="Model")

class BaseModel(Base, AllFeaturesMixin):
    __abstract__ = True

    @classmethod
    def find(cls, p_key):
        if p_key:
            return cls.query.filter(cls.id == int(p_key)).first()
        else:
            return None


def get_session(app):
    engine = create_engine('sqlite:///user.db', convert_unicode=True)
    return engine, scoped_session(
        sessionmaker(
            autocommit=False,
            autoflush=False,
            bind=engine
        )
    )

def init_db(app, engine, session):

    BaseModel.query = session.query_property()

    @app.teardown_appcontext
    def shutdown_session(exception=None):
        if exception:
            session.rollback()
        session.remove()

    BaseModel.metadata.create_all(bind=engine)


class User(BaseModel):
    __tablename__ = 'user'

    id = Column(Integer, primary_key=True)
    username = Column(String)
    password = Column(String)
    quota = Column(Integer, default=500)


app = Flask(__name__)
conn = cups.Connection()
printers = conn.getPrinters()
printer = 'PDF'

engine, session_db = get_session(app)
BaseModel.set_session(session_db)
init_db(app, engine, session_db)


class RegistrationForm(Form):
    username = StringField('Username', [validators.Length(min=8, max=24)])
    password = PasswordField('Password', [
        validators.DataRequired(),
        validators.EqualTo('confirm', message='Passwords do not match')
    ])
    confirm = PasswordField('Confirm Password')


def is_logged_in(f):
    @wraps(f)
    def wrap(*args, **kwargs):
        if 'logged_in' in session:
            return f(*args, **kwargs)
        else:
            flash('Access denied.', 'danger')
            return redirect(url_for('login'))

    return wrap

@app.route('/')
def home():
    return render_template('home.html')

@app.route('/jobs')
@is_logged_in
def jobs():
    all_jobs = conn.getJobs(my_jobs=True)

    new_jobs = {}

    return render_template('job_list.html', jobs=all_jobs)

@app.route('/register', methods=['GET', 'POST'])
def register():
    form = RegistrationForm(request.form)

    if request.method == 'POST' and form.validate():
        User.create(username=form.username.data, password=sha256_crypt.encrypt(str(form.password.data)))
        User.session.commit()

        flash('User added to database.', 'success')
        return redirect(url_for('home'))

    return render_template('register.html', form=form)

@app.route('/login', methods=['GET', 'POST'])
def login():
    if request.method == 'POST':
        username = request.form['username']
        password = request.form['password']

        user = User.query.filter_by(username=username).first()

        if user:
            pass_test = user.password

            if sha256_crypt.verify(password, pass_test):
                flash('Login successful!', 'success')
                session['logged_in'] = True
                session['username'] = username

                return redirect(url_for('home'))
        else:
            flash('User does not exist.', 'danger')

    return render_template('login.html')

@app.route('/logout')
def logout():
    session.clear()
    flash('Logged out successfully!', 'success')

    return redirect(url_for('login'))

@app.route('/release/<job_id>')
def release_job(job_id):
    os.system('lp -i {} -H resume'.format(str(job_id)))
    flash('Job printing...', 'success')

    return redirect(url_for('jobs'))

if __name__ == '__main__':
    app.secret_key = 'super secret key'
    app.run(debug=True)
